The FlexLexer.h require that being installed the file called
*<iostream.h>* in path of c++ headers
with following content(this file can be downloaded from here https://bitbucket.org/Rakhim/course/downloads)
```
#!c++

#include <iostream>
using namespace std;
```

Written in Visual Studio 2010.

*Examples of the input files.*

Also can be downloaded from here 
https://bitbucket.org/Rakhim/course/downloads

------signature.config--------
```
#!txt
predicate Prime 1
predicate Natural 1
function  Mulitplication 2
predicate Bigger  2
const FIVE
predicate Equal   2
```
-------------------------------

*"Any natural number that is bigger than five can be factorizated"*

------sentence.txt----------
```
#!txt
ANY number (Natural(number) & Bigger(number, FIVE) -> (EXIST anotherNumber  EXIST anotherNumber2 Prime(anotherNumber) -> Equal(number, 
Multiplication(anotherNumber, anotherNumber2))))
```
----------------------------

*The program can eat the kind of sentences that are represents the formula of predicate logic https://en.wikipedia.org/wiki/Predicate_logic.
The format of that sentences is described in grammar file and looks like in the example above :*