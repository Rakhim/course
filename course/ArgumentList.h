#pragma once
#include <vector>
#include "Argument.h"

class ArgumentList
{
  std::vector<Argument*> arguments;

public:
  ArgumentList(void);
  ~ArgumentList(void);
  ArgumentList(const ArgumentList &_al);

  ArgumentList& operator= (const ArgumentList &_ql);

  bool unify(const ArgumentList &right) const;
  void addArgument(Argument *_argument);
  virtual int getCount() const;
  virtual std::string toString() const;
  virtual std::set<std::string> getVariables() const;
  virtual void replaceVariable(std::string old_name, int id);
  virtual void setVariableTable(VariablesTable *_vt);
};
