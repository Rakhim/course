#include "VariablesTable.h"
#include <algorithm>

VariablesTable::VariablesTable(void)
{
}

VariablesTable::~VariablesTable(void) {
}

int VariablesTable::add(std::string value) {
  int id = find(value);
  if (-1 != id) {
    return id;
  }

  strings.push_back(value);
  return strings.size() - 1;
}

std::string VariablesTable::getByID(int id) const {
  if (id < 0 && id > strings.size()) return std::string();
  return strings[id];
}
  
int VariablesTable::find(const std::string &value) const {

  std::vector<std::string>::const_iterator it = 
    std::find(strings.begin(), strings.end(), value);
  if (it != strings.end()) {
    return std::distance(strings.begin(), it);
  }
  return -1;
}

void VariablesTable::replaceByID(const std::string &string, int ID) {
  if (ID < 0 && ID > strings.size()) return;
  strings[ID] = string;
}
