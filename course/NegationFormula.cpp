#include "NegationFormula.h"

void NegationFormula::fillPredicateTable(PredicateTable *_pt) {
  formula->fillPredicateTable(_pt);
}

void NegationFormula::replaceVariable(std::string old_name, int id) {
  formula->replaceVariable(old_name, id);
}

void NegationFormula::setVariableTable(VariablesTable *_vt) {
  formula->setVariableTable(_vt);
}
