#include "Function.h"
#include "Variable.h"
#include "Constant.h"

Function::Function(const std::string &_identifier, const ArgumentList &_argument):
  argument(_argument),
  identifier(_identifier)
{
}

Function::~Function(void) {
}


bool Function::unify(const Argument *const _argument) const {
  if (std::string(typeid(*_argument).name()) == "class Function") {
    return ((Function*)_argument)->identifier == identifier && 
        argument.unify(((Function*)_argument)->argument);
  } else if (std::string(typeid(*_argument).name()) == "class Variable") {
    return ((Variable*)_argument)->unify(this);
  }
  return false;
}

std::string Function::toString() const {
  return identifier
          + std::string("(")
          + argument.toString()
          + std::string(")");
}

int Function::getCount() const {
  return argument.getCount();
}


std::set<std::string> Function::getVariables() const {
  std::set<std::string> result;
  std::set<std::string> &vars = argument.getVariables();
  result.insert(vars.begin(), vars.end());
  return result;
}

void Function::replaceVariable(std::string old_name, int id) {
  argument.replaceVariable(old_name, id);
}

Function* Function::clone() const {
  return new Function(*this);
}
void Function::setVariableTable(VariablesTable *_vt) {
  argument.setVariableTable(_vt);
}
