#pragma once
#include <set>
#include "Node.h"

class Argument:
  public Node
{

public:
  virtual bool unify(const Argument * const argument) const = 0;
  virtual Argument* clone() const = 0;
  virtual int getCount() const = 0;
  virtual std::string toString() const = 0;
  virtual std::set<std::string> getVariables() const = 0;
  virtual ~Argument() {
  }
};