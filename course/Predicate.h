#pragma once
#include <string>
#include "ArgumentList.h"

class Predicate
{
  std::string name;
  ArgumentList argument;

public:
  Predicate(std::string _name, const ArgumentList &_argument);
  ~Predicate(void);

  std::string getName() const;  
  ArgumentList getArgument() const;
  bool operator== (const Predicate &_predicate) const;
};
