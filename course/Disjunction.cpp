#include "Disjunction.h"

#include <iterator>
#include <algorithm>

void Disjunction::setPredicateTable(PredicateTable *_pt) {
  pt = _pt;
}

Disjunction::Disjunction():
  constant(false)
{
}

std::string Disjunction::toString() const {
  std::string value("(");
  std::map<int, bool>::const_iterator it;
  std::map<int, bool>::const_iterator aux_it;
  for (it = variables.begin(); it != variables.end(); it++) {
    value += (it->second ? "!" : "");
    Predicate &_predicate = pt->getByID(it->first);
    value += _predicate.getName();
    value += _predicate.getArgument().getCount() ?
        std::string("(") + _predicate.getArgument().toString() += ")" : "";
    aux_it = it;
    aux_it++;
    if ((aux_it) != variables.end()) {
      value += " | ";
    }
  }
  value += ")";

  return value;
}

std::set<Disjunction> Disjunction::resolvent(const Disjunction &_disj) const {
  std::set<Disjunction> result;
  std::map<int, bool>::const_iterator i, j;

  for (i = variables.begin(); i != variables.end(); i++) {
    for (j = _disj.variables.begin(); j != _disj.variables.end(); j++) {
      // if their names are the same and
      // their arguments are unifiable and are contr-ary
      ArgumentList &left = pt->getByID(i->first).getArgument();
      ArgumentList &right = pt->getByID(j->first).getArgument();

      std::string left_name = pt->getByID(i->first).getName();
      std::string right_name = pt->getByID(j->first).getName();
      bool unifiable = (!left.getCount() && !right.getCount()) ?
          left_name == right_name : left.unify(right);

      if ((left_name == right_name) && unifiable && (i->second != j->second)) {
        Disjunction disjunction(*this);
        disjunction.variables.erase(i->first);
        for (
          std::map<int, bool>::const_iterator k = _disj.variables.begin();
          k != _disj.variables.end();
          k++
        ) {
          disjunction.insert(k->first, k->second);
        }
        disjunction.variables.erase(j->first);
        result.insert(disjunction);
      } // if unify
    }
  }
  return result;
}

void Disjunction::insert(int _id, bool _negation) {
  if (constant) return;

  std::map<int, bool>::iterator it = variables.find(_id);
  if (variables.end() == it) {
    variables[_id] = _negation;
  } else if (it->second == !_negation) {
    constant = true;
    variables.clear();
  }
}

Disjunction Disjunction::or(const Disjunction &_disj) const {
  Disjunction result;
  result.variables.insert(
    variables.begin(),
    variables.end()
  );
  std::map<int, bool>::const_iterator i;
  for (i = _disj.variables.begin(); i != _disj.variables.end(); i++) {
    result.insert(i->first, i->second);
  }
  return result;
}
  
std::set<Disjunction> Disjunction::negate() const {

  std::set<Disjunction> result;
  std::map<int, bool>::const_iterator i;
  for (i = variables.begin(); i != variables.end(); i++) {
    Disjunction disj;
    disj.setPredicateTable(pt);
    disj.insert(i->first, !i->second);
    result.insert(disj);
  }
  return result;
}

Disjunction::~Disjunction() {
}

bool Disjunction::empty() const {
  return variables.empty();
}

bool operator< (const Disjunction &left, const Disjunction &right) {
  std::map<int, bool>::const_iterator i;
  std::map<int, bool>::const_iterator j;

  for (
    i = left.variables.begin(), j = right.variables.begin();
    i != left.variables.end() && j != right.variables.end();
    i++, j++
  ) {
    if (i->first != j->first) {
      return i->first < j->first;
    } else if (i->second != j->second) {
      return i->second < j->second;
    }
  }
  return left.variables.size() < right.variables.size();
}
