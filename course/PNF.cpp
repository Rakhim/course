#include "PNF.h"

PNF::PNF(QuantifierList _quantifierList, Formula *_formula):
  prefix(_quantifierList),
  formula(_formula)
{
}

PNF::~PNF(void) {
  delete formula;
}

PNF::PNF(const PNF &pnf) {
  prefix = pnf.prefix;
  formula = pnf.formula->clone();
}

std::string PNF::toString() const {
  return prefix.toString() + formula->toString();
}

SNF PNF::getSNF() const {
  return SNF(prefix, formula->getCNF());
}
