#include "AnyQuantifier.h"
#include "ExistQuantifier.h"

AnyQuantifier::AnyQuantifier(const std::string &_identifier):
  identifier(_identifier),
  id(-1)
{
}

AnyQuantifier::~AnyQuantifier(void) { 
}

Quantifier* AnyQuantifier::negate() const {
  return new ExistQuantifier(identifier);
}

std::string AnyQuantifier::toString() const {
  return std::string(" ANY ") + (-1 == id ? identifier : vt->getByID(id));
}

std::set<std::string> AnyQuantifier::getVariables() const {
  std::set<std::string> result;
  result.insert(identifier);
  return result;
}

AnyQuantifier* AnyQuantifier::clone() const {
  return new AnyQuantifier(*this);
}

void AnyQuantifier::replaceVariable(std::string old_name, int _id) {
  if (old_name == identifier) {
    id = _id;
  }
}

void AnyQuantifier::setVariableTable(VariablesTable *_vt) {
  vt = _vt;
}