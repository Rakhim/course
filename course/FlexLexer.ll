%{
#include "Parser.h"
%}

%option noyywrap
%option yyclass="Lexer"
%option yylineno

%%
";"       { return ';'; }
"("       { return '('; }
")"       { return ')'; }
","       { return ','; }
"|"       { return '|'; }
"&"       { return '&'; }
"!"       { return '!'; }
"~"       { return '~'; }
"->"      { return yy::BisonParser::token::IMPLICATION; }
"ANY"     { return yy::BisonParser::token::ANY;        }
"EXIST"   { return yy::BisonParser::token::EXIST;      }

[a-zA-Z]+[a-zA-Z0-9]* { return yy::BisonParser::token::ID; }
.               { }
\n              { }

%%
