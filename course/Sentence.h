#pragma once
#include "node.h"
#include "PNF.h"
#include "Formula.h"

class Sentence:
  public Node
{
public:
  virtual Formula* getFormula() const = 0;
  virtual Sentence* clone() const = 0;
  virtual Sentence* negate() const = 0;
  virtual std::string toString() const = 0; 
  virtual std::set<std::string> getVariables() const = 0;
  virtual PNF getPNF() const = 0;
  virtual ~Sentence(void) {
  }
};
