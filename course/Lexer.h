#pragma once
#include <FlexLexer.h>

#define YY_DECL int Lexer::yylex()

class Lexer :
  public yyFlexLexer
{
public:
  YY_DECL;
};
