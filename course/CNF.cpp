#include "CNF.h"

CNF::CNF(PredicateTable *_pt):
  pt(_pt)
{
}

std::set<Disjunction> CNF::getSetOfDisjunctions() const {
  return disjuncts;
}

void CNF::insert(const Disjunction &_disjunct) {
  Disjunction dis(_disjunct);
  dis.setPredicateTable(pt);
  disjuncts.insert(dis);
}

void CNF::insert(const std::set<Disjunction> &_disjuncts) {
  disjuncts.insert(_disjuncts.begin(), _disjuncts.end());
}
  
CNF CNF::or(const CNF &_cnf) const {
  CNF result(pt);
  if (disjuncts.empty()) {
    result.disjuncts.insert(
      _cnf.disjuncts.begin(),
      _cnf.disjuncts.end()
    );
    return result;
  } else if (_cnf.disjuncts.empty()) {
    result.disjuncts.insert(
      disjuncts.begin(),
      disjuncts.end()
    );
    return result;
  }

  std::set<Disjunction>::iterator i;
  std::set<Disjunction>::iterator j;
  for (i = disjuncts.begin(); i != disjuncts.end(); i++) {
    for (j = _cnf.disjuncts.begin(); j != _cnf.disjuncts.end(); j++) {
      result.insert(i->or(*j));
    }
  }
  return result;
}

CNF CNF::and(const CNF &_cnf) const {
  CNF result(pt);
  std::set<Disjunction>::iterator j;
  result.disjuncts.insert(disjuncts.begin(), disjuncts.end());
  for (j = _cnf.disjuncts.begin(); j != _cnf.disjuncts.end(); j++) {
    result.insert(*j);
  }
  return result;
}

CNF CNF::negate() const {
  CNF result(pt);

  std::set<Disjunction>::iterator i;
  for (i = disjuncts.begin(); i != disjuncts.end(); i++) {
    CNF _result(pt);
    _result.insert(i->negate());
    result = result.or(_result);
  }
  return result;
}

std::string CNF::toString() const {
  std::string result;
  std::set<Disjunction>::iterator i;
  for (i = disjuncts.begin(); i != disjuncts.end(); i++) {
    result += i->toString();
  }
  return result;
}

CNF::~CNF() {
}
