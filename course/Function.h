#pragma once
#include "term.h"
#include "ArgumentList.h"

class Function:
  public Term
{
  std::string identifier;
  ArgumentList argument;
  
public:
  Function(const std::string &_identifier, const ArgumentList &_argument);
  ~Function(void);

  virtual bool unify(const Argument * const _function) const;
  virtual std::string toString() const;
  virtual int getCount() const;
  std::set<std::string> getVariables() const;
  virtual void replaceVariable(std::string old_name, int id);
  virtual void setVariableTable(VariablesTable *_vt);
  virtual Function* clone() const;
};
