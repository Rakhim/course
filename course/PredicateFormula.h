#pragma once
#include <sstream>
#include <map>

#include "BaseFormula.h"
#include "Lexer.h"
#include "Parser.h"
#include "Signature.h"

class PredicateFormula:
   public BaseFormula
 {
  Parser *parser;
  Lexer *lexer;
  Sentence *sentence;
  Signature signature;

private:
  void constructor(std::istream &_signature, std::istream &_sentence);
  void constructor(std::istream &_sentence);

  PredicateFormula(const PredicateFormula &pf) {
  }
  
public:
  PredicateFormula(const Signature &_signature, const std::string &_sentence);
  PredicateFormula(const Signature &_signature, std::istream &_sentence);

 ~PredicateFormula();

  SNF getSNF() const;

  std::string toString() const;
  void fillPredicateTable(PredicateTable &_pt) const;
};