#pragma once
#include "BaseFormula.h"
#include "QuantifierList.h"
#include "Formula.h"
#include "SNF.h"

class PNF:
  public BaseFormula
{
  QuantifierList prefix;
  Formula *formula;

public:
  PNF(QuantifierList _quantifierList, Formula *_formula);
  PNF(const PNF &pnf);
  ~PNF(void);

  virtual std::string toString() const;
  SNF getSNF() const;
};
