#pragma once
#include <vector>
#include <string>

class VariablesTable
{
  std::vector<std::string> strings;

public:
  VariablesTable(void);
  ~VariablesTable(void);

  int add(std::string string);
  std::string getByID(int id) const;
  int find(const std::string &string) const;
  void replaceByID(const std::string &string, int ID);
 
  std::vector<std::string> getStrings() const {
    return strings;
  }
};
