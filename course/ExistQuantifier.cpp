#include "ExistQuantifier.h"
#include "AnyQuantifier.h"

ExistQuantifier::ExistQuantifier(const std::string &_identifier):
  identifier(_identifier),
  id(-1)
{
}

ExistQuantifier::~ExistQuantifier(void) {
}


Quantifier* ExistQuantifier::negate() const {
  return new AnyQuantifier(identifier);
}

std::string ExistQuantifier::toString() const {
  return std::string(" EXIST ") + (-1 == id ? identifier : vt->getByID(id));
}

std::set<std::string> ExistQuantifier::getVariables() const {
  std::set<std::string> result;
  result.insert(identifier);
  return result;
}

ExistQuantifier* ExistQuantifier::clone() const {
  return new ExistQuantifier(*this);
}

void ExistQuantifier::replaceVariable(std::string old_name, int _id) {
  if (identifier == old_name) {
    id = _id;
  }
}

void ExistQuantifier::setVariableTable(VariablesTable *_vt) {
  vt = _vt;
}
