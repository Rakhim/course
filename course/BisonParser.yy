%skeleton "./lalr1.cc"
%no-lines
%define parser_class_name "BisonParser"
%debug

%union {
  std::string *text;
  ArgumentList *arguments;
  Argument *argument;
  QuantifierList *quantifiers;
  Quantifier *quantifier;
  Sentence *sentence;
  Formula *formula;
}

%code requires {
#include "Implementations.h"
#include "Signature.h"
#include <algorithm>
}

%parse-param {Sentence* &formula}
%parse-param {Signature* signature}

%token IMPLICATION ANY EXIST
%token <text> ID

%type <sentence> sentence
%type <formula> formula quantifierless atom
%type <arguments> arg
%type <argument> term
%type <quantifiers> quantifiers
%type <quantifier> quantifier

%%

start: sentence ';'
{
  formula = $1;
  
  std::set<std::string> result;
  std::set<std::string> &first = formula->getVariables();
  std::set<std::string> &second = signature->getConstants();
  std::set_difference(
    first.begin(), first.end(),
    second.begin(), second.end(),
    std::inserter(result, result.begin())
  );

  printf("%s ", (result.size()) ? "Warning : the sentence consits free variables" : "");
  
  std::set<std::string>::iterator it;
  if (result.size()) {
    printf(" [");
    for (it = result.begin(); it != result.end();it++) {
      printf("%s ", (*it).c_str());
    }
    printf("]\n\n");
  }
  return 0;
}

sentence: formula
{
  $$ = new QuantifierSentence(QuantifierList(), $1);
}
    | quantifiers formula  
{
  $$ = new QuantifierSentence(*$1, $2);
  delete $1;
}

formula: quantifierless
{
  $$ = $1;
}
    | formula '|' quantifierless
{
  $$ = new DisjunctionFormula($1, $3);
}
    | formula '~' quantifierless
{
  $$ = new EquationFormula($1, $3);
}
    | formula IMPLICATION quantifierless    
{
  $$ = new ImplicationFormula($1, $3);
}

quantifierless:  atom
{
  $$ = $1;
}
    | quantifierless '&' atom
{
  $$ = new ConjunctionFormula($1, $3);
}

atom: ID 
{
  $$ = new Atom(*$1, ArgumentList());

  if (0 == signature->findPredicate(*$1)) {
  } else {
    printf("Warning : constant predicate \"%s\" is not defined\n", $1->c_str());
  }
  delete $1;
}
    | ID '(' arg ')'
{
  $$ = new Atom(*$1, *$3);

  int countOfArgs = signature->findPredicate(*$1);
  if (countOfArgs) {
    if ($3->getCount() == countOfArgs) {
      
    } else {
       printf("Warning : predicate \"%s\" have invalid count of args.Must be %d instead of %d\n", $1->c_str(), countOfArgs, $3->getCount());
    }
  } else {
    printf("Warning : predicate \"%s\" is not defined\n", $1->c_str());
  }
  delete $1;
  delete $3;
}
    | '(' sentence ')'
{
  $$ = new ParenthsizedFormula($2);
}
    | '!' atom
{
  $$ = new NegationFormula($2);
}
 
arg: term
{
  $$ = new ArgumentList();
  ((ArgumentList*)$$)->addArgument($1);
}
    | arg ',' term
{
  $$ = $1;
  ((ArgumentList*)$$)->addArgument($3);
}

term: ID
{
  if (signature->findConstant(*$1)) {
    $$ = new Constant(*$1);
  } else {
    $$ = new Variable(*$1);
  }
  delete $1;
}
    | ID '(' arg ')'
{
  $$ = new Function(*$1, *$3);
  int countOfArgs = signature->findFunction(*$1);
  if (countOfArgs) {
    if ($3->getCount() == countOfArgs) {
      
    } else {
       printf("Warning : function \"%s\" have invalid count of args.Must be %d instead of %d\n", $1->c_str(), countOfArgs, $3->getCount());
    }
  } else {
    printf("Warning : function \"%s\" is not defined\n", $1->c_str());
  }
  delete $1;
  delete $3;
}
    | '(' term ')'
{
  $$ = $2;
}

quantifiers: quantifier
{
  $$ = new QuantifierList();
  $$->addQuantifier($1);
}
    | quantifiers quantifier
{
  $$ = $1;
  $$->addQuantifier($2);
}

quantifier: ANY ID
{
  $$ = new AnyQuantifier(*$2);
  delete $2;
}
    | EXIST ID
{
  $$ = new ExistQuantifier(*$2);
  delete $2;
}

%%

void yy::BisonParser::error(class yy::location const &,class std::basic_string<char,struct std::char_traits<char>,class std::allocator<char> > const &)
{
}