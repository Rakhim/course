#pragma once

#include "QuantifierSentence.h"

#include "DisjunctionFormula.h"
#include "EquationFormula.h"
#include "ImplicationFormula.h"
#include "ConjunctionFormula.h"

#include "Atom.h"
#include "ParenthsizedFormula.h"
#include "NegationFormula.h"

#include "ArgumentList.h"

#include "AnyQuantifier.h"
#include "ExistQuantifier.h"
#include "QuantifierList.h"

#include "Function.h"
#include "Variable.h"
#include "Constant.h"