#pragma once
#include "BaseFormula.h"
#include "Disjunction.h"

class CNF:
  public BaseFormula
{
  std::set<Disjunction> disjuncts;
  PredicateTable *pt;

private:
  void insert(const std::set<Disjunction> &_disjuncts);

public:

  CNF(PredicateTable *_pt);
  ~CNF();

  std::set<Disjunction> getSetOfDisjunctions() const;
  void insert(const Disjunction &_disjunct);
  
  CNF or(const CNF &_cnf) const;
  CNF and(const CNF &_cnf) const;
  CNF negate() const;

  std::string toString() const;
};