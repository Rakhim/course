#pragma once
#include "Formula.h"
#include "ArgumentList.h"

class Atom:
  public Formula
{
  int id;
  std::string identifier;
  ArgumentList argument;
  PredicateTable *pt;

public:
  Atom(const std::string &_identifier, const ArgumentList &_argument);

  ~Atom(void) {
  }

  virtual Formula* negate() const;
  virtual std::string toString() const;
  int getCountOfArguments() const;
  virtual std::set<std::string> getVariables() const;
  virtual Atom* clone() const;
  virtual CNF getCNF() const;
  virtual void fillPredicateTable(PredicateTable *_pt);
  virtual void replaceVariable(std::string old_name, int id);
  virtual void setVariableTable(VariablesTable *_vt);
};
