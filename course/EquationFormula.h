#pragma once
#include "formula.h"

class EquationFormula :
  public Formula
{
  Formula *left;
  Formula *right;
  
public:
  EquationFormula(Formula *_left, Formula *_right);
  EquationFormula(const EquationFormula &formula);
  ~EquationFormula(void);
  virtual EquationFormula* clone() const;  virtual CNF getCNF() const;
  virtual std::string toString() const;
  virtual std::set<std::string> getVariables() const;
  virtual Formula* negate() const;
  virtual void fillPredicateTable(PredicateTable *_pt);
  virtual void replaceVariable(std::string old_name, int id);
  virtual void setVariableTable(VariablesTable *_vt);
};
