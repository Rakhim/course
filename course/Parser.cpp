#include "Parser.h"

Parser::Parser(Lexer *_lexer, Sentence *&_sentence, Signature* _signature):
  yy::BisonParser(_sentence, _signature),
  lexer(_lexer)
{
}

Parser::~Parser(void) {
}

int Parser::yylex(yy::BisonParser::semantic_type *p) {
  int tokenType = lexer->yylex();
  switch (tokenType) {
    case yy::BisonParser::token::ID:
      p->text = new std::string(lexer->YYText());        
      break;
    default:
      ;
  }
  return tokenType;
}