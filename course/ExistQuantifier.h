#pragma once
#include "quantifier.h"

class ExistQuantifier :
  public Quantifier
{
  std::string identifier;
  VariablesTable *vt;
  int id;

public:
  ExistQuantifier(const std::string &_identifier);
  ~ExistQuantifier(void);
  virtual ExistQuantifier* clone() const;
  virtual std::string toString() const;
  virtual std::set<std::string> getVariables() const;
  virtual Quantifier* negate() const;
  virtual void replaceVariable(std::string old_name, int id);
  virtual void setVariableTable(VariablesTable *_vt);

};
