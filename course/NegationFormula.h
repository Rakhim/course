#pragma once
#include "Formula.h"

class NegationFormula:
  public Formula
{
  Formula *formula;

public:
  NegationFormula(Formula *_formula):
    formula(_formula)
  {
  }
  NegationFormula(const NegationFormula &_formula) {
    formula = _formula.formula->clone();
  }
  ~NegationFormula(void) {
    delete formula;
  }

  virtual CNF getCNF() const {
    return formula->getCNF().negate();
  }
  virtual std::string toString() const {
    return std::string("!") + formula->toString();
  }
  virtual std::set<std::string> getVariables() const {
    return formula->getVariables();
  }
  virtual Formula* negate() const {
    return formula;
  }
  virtual NegationFormula* clone() const {
    return new NegationFormula(*this);
  }
  virtual void fillPredicateTable(PredicateTable *_pt);
  virtual void replaceVariable(std::string old_name, int id);
  virtual void setVariableTable(VariablesTable *_vt);
};
