#pragma once
#include <set>

#include "Node.h"

class Quantifier:
  public Node
{
public:
  virtual Quantifier* clone() const = 0;
  virtual Quantifier* negate() const = 0;
  virtual ~Quantifier() {
  }
};
