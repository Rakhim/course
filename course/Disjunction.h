﻿#pragma once
#include <set>
#include <map>

#include "BaseFormula.h"

class Disjunction:
  public BaseFormula
{
  // id in the PredicateTable & is negative
  std::map<int, bool> variables;
  bool constant;
  PredicateTable *pt;

public:
  void setPredicateTable(PredicateTable *_pt);
  Disjunction();
  ~Disjunction();

  std::string toString() const;
  std::set<Disjunction> resolvent(const Disjunction &_dis) const;
  void insert(int _id, bool _negation);
  Disjunction or(const Disjunction &_disj) const;
  std::set<Disjunction> negate() const;
  bool empty() const;
  friend bool operator< (const Disjunction &left, const Disjunction &right);
};
