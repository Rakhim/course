#include "PredicateTable.h"
#include <algorithm>

PredicateTable::PredicateTable(void)
{
}

PredicateTable::~PredicateTable(void)
{
}

Predicate PredicateTable::getByID(int id) const {
  return predicates[id];
}

int PredicateTable::add(const Predicate &_predicate) {
  std::vector<Predicate>::iterator it = std::find(predicates.begin(), predicates.end(), _predicate);
  if (it == predicates.end()) {
    predicates.push_back(_predicate);
    return predicates.size() - 1;
  }
  return std::distance(predicates.begin(), it);
}

void PredicateTable::replaceByID(const Predicate &_predicate, int ID) {
  predicates[ID] = _predicate;
}

int PredicateTable::find(const Predicate &_predicate) const {
  std::vector<Predicate>::const_iterator it = std::find(
    predicates.begin(),
    predicates.end(),
    _predicate
  );
  return predicates.end() != it ? std::distance(predicates.begin(), it) : -1;
}
