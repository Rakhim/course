#include "QuantifierList.h"

QuantifierList::QuantifierList(void) {
}

QuantifierList::QuantifierList(const QuantifierList &_quantifiersList) {
  std::vector<Quantifier*>::const_iterator it;
  for (it = _quantifiersList.quantifiers.begin();
    it != _quantifiersList.quantifiers.end(); it++
  ) {
    quantifiers.push_back((*it)->clone());
  }
}

QuantifierList::~QuantifierList(void)
{
  std::vector<Quantifier*>::const_iterator it;
  for (it = quantifiers.begin(); it != quantifiers.end(); it++) {
    delete *it;
  }
}

QuantifierList& QuantifierList::operator= (const QuantifierList &_ql) {
  std::vector<Quantifier*>::const_iterator ij;
  for (ij = quantifiers.begin(); ij != quantifiers.end(); ij++) {
    delete (*ij);
  }
  quantifiers.clear();

  std::vector<Quantifier*>::const_iterator it;
  for (it = _ql.quantifiers.begin();
    it != _ql.quantifiers.end(); it++
  ) {
    quantifiers.push_back((*it)->clone());
  }
  return *this;
}


void QuantifierList::addQuantifier(Quantifier *_quantifier) {
  quantifiers.push_back(_quantifier);
}

std::string QuantifierList::toString() const {
  std::string result;
  std::vector<Quantifier*>::const_iterator it;
  for (it = quantifiers.begin(); it != quantifiers.end(); it++) {
    result += (*it)->toString();
    result += " ";
  }
  return result;
}
  
std::set<std::string> QuantifierList::getVariables() const {
  std::set<std::string> result;
  std::vector<Quantifier*>::const_iterator it;
  for (it = quantifiers.begin(); it != quantifiers.end(); it++) {
    std::set<std::string> vars = (*it)->getVariables();
    result.insert(vars.begin(), vars.end());
  }
  return result;
}

QuantifierList QuantifierList::negate() const {
  QuantifierList result;
  std::vector<Quantifier*>::const_iterator it;
  for (it = quantifiers.begin(); it != quantifiers.end(); it++) {
    result.addQuantifier((*it)->negate());
  }
  return result;
}

void QuantifierList::replaceVariable(std::string old_name, int id) {
  std::vector<Quantifier*>::iterator it;
  for (it = quantifiers.begin(); it != quantifiers.end(); it++) {
    (*it)->replaceVariable(old_name, id);
  }
}

void QuantifierList::setVariableTable(VariablesTable *_vt) {
  std::vector<Quantifier*>::const_iterator it;
  for (it = quantifiers.begin(); it != quantifiers.end(); it++) {
    (*it)->setVariableTable(_vt);
  }
}
