#include "Predicate.h"

Predicate::Predicate(std::string _name, const ArgumentList &_argument):
  name(_name),
  argument(_argument)
{
}

Predicate::~Predicate(void) {
}

std::string Predicate::getName() const {
  return name;
}

ArgumentList Predicate::getArgument() const {
  return argument;
}

bool Predicate::operator== (const Predicate &_predicate) const {
  return (name == _predicate.name ?
          argument.toString() == _predicate.getArgument().toString() : false);
}
