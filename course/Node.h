#pragma once
#include <string>
#include <set>

#include "VariablesTable.h"

class Node {
public:
  virtual Node* clone() const = 0;
  virtual void setVariableTable(VariablesTable *_vt) = 0;
  virtual void replaceVariable(std::string old_name, int id) = 0;
  virtual std::string toString() const = 0;
  virtual std::set<std::string> getVariables() const = 0;
  virtual ~Node() {
  }
};