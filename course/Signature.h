#pragma once
#include <string>
#include <map>
#include <set>

class Signature
{
  // name & arity
  std::map<std::string, int> predicates;
  std::map<std::string, int> functions;
  std::set<std::string> constants;

public:
  Signature(void);
  ~Signature(void);

  void read(std::istream &_signature);

  void addPredicate(std::string name, int params_count);
  void addFunction(std::string name, int params_count);
  void addConstant(std::string name);

  int findPredicate(std::string name);
  int findFunction(std::string name);
  bool findConstant(std::string name);
  std::set<std::string> getConstants();
};
