#pragma once
#include "BisonParser.tab.hh"
#include "Lexer.h"
#include "Signature.h"

class Parser :
  public yy::BisonParser
{
  Lexer *lexer;
public:
  Parser(Lexer *_lexer, Sentence *&_sentence, Signature* _signature);

  ~Parser(void);
  int yylex(yy::BisonParser::semantic_type *p);
};
