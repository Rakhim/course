#include "QuantifierSentence.h"
#include "NegationFormula.h"

int QuantifierSentence::addToVariablesTableWithRename(VariablesTable &vt, const std::string &_name) const {
  std::string name = _name;
  long long number = 1;
  while(true) {
    if (-1 == vt.find(name)) {
      return vt.add(name);
    } else {
      name = _name + std::to_string(number);
    }
    ++number;
  }
}

QuantifierSentence::QuantifierSentence(const QuantifierList &_quantifier, Formula *_formula):
  quantifier(_quantifier),
  formula(_formula)
{
}

QuantifierSentence::QuantifierSentence(const QuantifierSentence &qs) {
  quantifier = qs.quantifier;
  formula = qs.formula->clone();
}

QuantifierSentence::~QuantifierSentence(void) {
  delete formula;
}
  
QuantifierSentence* QuantifierSentence::clone() const {
  return new QuantifierSentence(*this);
}

Sentence* QuantifierSentence::negate() const {
  return new QuantifierSentence(quantifier.negate(), formula->negate());
}

void QuantifierSentence::fillPredicateTable(PredicateTable *_pt) {
  formula->setVariableTable(&_pt->variablesTable());
  quantifier.setVariableTable(&_pt->variablesTable());
  formula->fillPredicateTable(_pt);

  std::set<std::string> formulaVars = formula->getVariables();
  std::set<std::string> quantifierVars = quantifier.getVariables();

  std::set<std::string> intersectionVars;
  std::set_intersection(
    formulaVars.begin(), formulaVars.end(),
    quantifierVars.begin(), quantifierVars.end(),
    std::inserter(intersectionVars, intersectionVars.begin())
  );
  int new_id;
  VariablesTable &vt = _pt->variablesTable();
  std::set<std::string>::const_iterator it = intersectionVars.begin();
  for (; it != intersectionVars.end(); it++) {
    new_id = addToVariablesTableWithRename(vt, *it);
    formula->replaceVariable(*it, new_id);
    quantifier.replaceVariable(*it, new_id);
  }
}

CNF QuantifierSentence::getCNF() const {
  return formula->getCNF();
}

std::string QuantifierSentence::toString() const {
  return quantifier.toString() + std::string(" ") + formula->toString();
}

Quantifier* QuantifierSentence::getPrefix() const {
  return 0;
}

std::set<std::string> QuantifierSentence::getVariables() const {
  std::set<std::string> result;
  std::set<std::string> &first = formula->getVariables();
  std::set<std::string> &second = quantifier.getVariables();
  std::set_difference(
    first.begin(), first.end(),
    second.begin(), second.end(),
    std::inserter(result, result.begin())
  );
  return result;
}

void QuantifierSentence::replaceVariable(std::string old_name, int id) {
  quantifier.replaceVariable(old_name, id);
  formula->replaceVariable(old_name, id);
}

void QuantifierSentence::setVariableTable(VariablesTable *_vt) {
  quantifier.setVariableTable(_vt);
  formula->setVariableTable(_vt);
}

PNF QuantifierSentence::getPNF() const {
  return PNF(quantifier, formula->clone());
}

Formula* QuantifierSentence::getFormula() const {
  return formula;
}
