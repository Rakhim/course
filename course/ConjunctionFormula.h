#pragma once
#include "Formula.h"

class ConjunctionFormula:
  public Formula
{
  Formula *left;
  Formula *right;
  
public:
  ConjunctionFormula(Formula *_left, Formula *_right);
  ConjunctionFormula(const ConjunctionFormula &formula);
  ~ConjunctionFormula(void);
  virtual CNF getCNF() const;
  virtual std::string toString() const;
  virtual std::set<std::string> getVariables() const;
  virtual ConjunctionFormula* clone() const;
  virtual Formula* negate() const;
  virtual void fillPredicateTable(PredicateTable *_pt);
  virtual void replaceVariable(std::string old_name, int id);
  virtual void setVariableTable(VariablesTable *_vt);

};
