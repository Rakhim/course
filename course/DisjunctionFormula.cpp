#include "DisjunctionFormula.h"
#include "ConjunctionFormula.h"

DisjunctionFormula::DisjunctionFormula(Formula *_left, Formula *_right):
  left(_left),
  right(_right)
{
}

DisjunctionFormula::DisjunctionFormula(const DisjunctionFormula &formula) {
  left = formula.left->clone();
  right = formula.right->clone();
}

DisjunctionFormula::~DisjunctionFormula(void) {
  delete left;
  delete right;
}

CNF DisjunctionFormula::getCNF() const {
  return left->getCNF()
          .or(right->getCNF());
}

std::string DisjunctionFormula::toString() const {
  return left->toString() + std::string(" | ") + right->toString();
}

std::set<std::string> DisjunctionFormula::getVariables() const {
  std::set<std::string> &leftVars = right->getVariables();
  std::set<std::string> &rightVars = left->getVariables();

  std::set<std::string> result;
  result.insert(leftVars.begin(), leftVars.end());
  result.insert(rightVars.begin(), rightVars.end());
    
  return result;
}

DisjunctionFormula* DisjunctionFormula::clone() const {
  return new DisjunctionFormula(*this);
}

Formula* DisjunctionFormula::negate() const {
  return new ConjunctionFormula(
    left->negate(),
    right->negate()
  );
}

void DisjunctionFormula::fillPredicateTable(PredicateTable *_pt) {
  left->fillPredicateTable(_pt);
  right->fillPredicateTable(_pt);
}

void DisjunctionFormula::replaceVariable(std::string old_name, int id) {
  left->replaceVariable(old_name, id);
  right->replaceVariable(old_name, id);
}

void DisjunctionFormula::setVariableTable(VariablesTable *_vt) {
  left->setVariableTable(_vt);
  right->setVariableTable(_vt);
}
