#include "SNF.h"

SNF::SNF(const QuantifierList &quantifierList, const CNF &cnf):
  prefix(quantifierList),
  matrix(cnf)
{
}

SNF::~SNF(void)
{
}
