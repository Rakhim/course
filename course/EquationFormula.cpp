#include "EquationFormula.h"
#include "DisjunctionFormula.h"
#include "ConjunctionFormula.h"

EquationFormula::EquationFormula(const EquationFormula &formula) {
  left = formula.left->clone();
  right = formula.right->clone();
}

EquationFormula* EquationFormula::clone() const {
  return new EquationFormula(*this);
}

EquationFormula::EquationFormula(Formula *_left, Formula *_right):
  left(_left),
  right(_right)
{
}

EquationFormula::~EquationFormula(void) {
  delete left;
  delete right;
}

Formula* EquationFormula::negate() const {
  return new DisjunctionFormula(
    new ConjunctionFormula(left->negate(), right),
    new ConjunctionFormula(left, right->negate())
  );
}

void EquationFormula::fillPredicateTable(PredicateTable *_pt) {
  left->fillPredicateTable(_pt);
  right->fillPredicateTable(_pt);
}

void EquationFormula::replaceVariable(std::string old_name, int id) {
  left->replaceVariable(old_name, id);
  right->replaceVariable(old_name, id);
}

CNF EquationFormula::getCNF() const {
  return left->getCNF()
    .and(right->getCNF())
    .or(
        left->getCNF().negate()
        .and(
          right->getCNF().negate()
        )
    );
}


void EquationFormula::setVariableTable(VariablesTable *_vt) {
  left->setVariableTable(_vt);
  right->setVariableTable(_vt);  
}

std::string EquationFormula::toString() const {
  return left->toString() + std::string(" ~ ") + right->toString();
}

std::set<std::string> EquationFormula::getVariables() const {
  std::set<std::string> &leftVars = left->getVariables();
  std::set<std::string> &rightVars = right->getVariables();

  std::set<std::string> result;
  result.insert(leftVars.begin(), leftVars.end());
  result.insert(rightVars.begin(), rightVars.end());
    
  return result;
}
