#include "Signature.h"

Signature::Signature(void) {
}

void Signature::read(std::istream &_signature) {
  std::string name;
  std::string type;
  int params_count;

  while (_signature) {
    _signature >> type;
    _signature >> name;
      
    if (!type.compare("predicate")) {
      _signature >> params_count;
      addPredicate(name, params_count);
    } else if (!type.compare("function")) {
      _signature >> params_count;
      addFunction(name, params_count);
    } else if (!type.compare("const")) {
      addConstant(name);
    } else {
      // wrong type
    }
  }
}

Signature::~Signature(void)
{
  predicates.clear();
  functions.clear();
  constants.clear();
}

void Signature::addPredicate(std::string name, int params_count) {
  predicates[name] = params_count;
}

void Signature::addFunction(std::string name, int params_count) {
  functions[name] = params_count;
}

void Signature::addConstant(std::string name) {
  constants.insert(name);
}

int Signature::findPredicate(std::string name) {
  std::map<std::string, int>::iterator i = predicates.find(name);
  if (i != predicates.end()) {
    return i->second;
  }
  return 0;
}

int Signature::findFunction(std::string name) {
  std::map<std::string, int>::iterator i = functions.find(name);
  if (i != functions.end()) {
    return i->second;
  }
  return 0;
}

bool Signature::findConstant(std::string name) {
  return constants.end() != constants.find(name);
}

std::set<std::string> Signature::getConstants() {  
  return constants;
}