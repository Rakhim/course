#pragma once
#include "term.h"

class Constant :
  public Term
{
  std::string identifier;
  VariablesTable *vt;
  int id;

public:
  Constant(const std::string &_identifier);
  ~Constant(void);

  virtual bool unify(const Argument * const _variable) const;
  virtual Constant* clone() const;
  virtual int getCount() const;
  virtual std::string toString() const;
  virtual std::set<std::string> getVariables() const;
  virtual void replaceVariable(std::string old_name, int id);
  virtual void setVariableTable(VariablesTable *_vt);
};

