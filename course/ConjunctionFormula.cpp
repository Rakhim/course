#include "ConjunctionFormula.h"
#include "DisjunctionFormula.h"

ConjunctionFormula::ConjunctionFormula(Formula *_left, Formula *_right):
  left(_left),
  right(_right)
{
}

ConjunctionFormula::ConjunctionFormula(const ConjunctionFormula &formula) {
  left = formula.left->clone();
  right = formula.right->clone();
}

ConjunctionFormula::~ConjunctionFormula(void) {
  delete left;
  delete right;
}

CNF ConjunctionFormula::getCNF() const {
  return left->getCNF().and(right->getCNF());
}

std::string ConjunctionFormula::toString() const {
  return left->toString() + std::string(" & ") + right->toString();
}

std::set<std::string> ConjunctionFormula::getVariables() const {
  std::set<std::string> &leftVars = left->getVariables();
  std::set<std::string> &rightVars = right->getVariables();

  std::set<std::string> result;
  result.insert(leftVars.begin(), leftVars.end());
  result.insert(rightVars.begin(), rightVars.end());
    
  return result;
}

ConjunctionFormula* ConjunctionFormula::clone() const {
  return new ConjunctionFormula(*this);
}

Formula* ConjunctionFormula::negate() const {
  return new DisjunctionFormula(
    left->negate(),
    right->negate()
  );
}

void ConjunctionFormula::fillPredicateTable(PredicateTable *_pt) {
  left->fillPredicateTable(_pt);
  right->fillPredicateTable(_pt);
}

void ConjunctionFormula::replaceVariable(std::string old_name, int id) {
  left->replaceVariable(old_name, id);
  right->replaceVariable(old_name, id);
}

void ConjunctionFormula::setVariableTable(VariablesTable *_vt) {
  left->setVariableTable(_vt);
  right->setVariableTable(_vt);
}