#include "ArgumentList.h"
#include "Function.h"
#include "Constant.h"
#include "Variable.h"

ArgumentList::ArgumentList(void)
{
}

ArgumentList::ArgumentList(const ArgumentList &_al) {
  std::vector<Argument*>::const_iterator it;
  for (it = _al.arguments.begin(); (it) != _al.arguments.end(); it++) {
    arguments.push_back((*it)->clone());
  }
}


ArgumentList::~ArgumentList(void) {
  std::vector<Argument*>::iterator it;
  for (it = arguments.begin(); (it) != arguments.end(); it++) {
    delete *it;
  }
  arguments.clear();
}

bool ArgumentList::unify(const ArgumentList &right) const {
  return true;
  std::vector<Argument*>::const_iterator i, j;
  for (
    i = arguments.begin(), j = right.arguments.begin();
    (i != arguments.end()) && (j != right.arguments.end());
    i++, j++) {

    if (std::string(typeid(*(*i)).name()) == "class Function") {
      if(!((Function*)(*i))->unify((Function*)(*j))) return false;
    } else if (std::string(typeid(*(*i)).name()) == "class Variable") {
      if(!((Variable*)(*i))->unify((Variable*)(*j))) return false;  
    } else if (std::string(typeid(*(*i)).name()) == "class Constant") {
      if(!((Constant*)(*i))->unify((Constant*)(*j))) return false;
    } else {
      printf("The type of the pointer with the type of the base class called Argument is not classified!\n");
    }
  }
  return true;
}

ArgumentList& ArgumentList::operator= (const ArgumentList &_al) {
  std::vector<Argument*>::iterator it;
  for (it = arguments.begin(); (it) != arguments.end(); it++) {
    delete *it;
  }  
  arguments.clear();

  std::vector<Argument*>::const_iterator ij;
  for (ij = _al.arguments.begin(); (ij) != _al.arguments.end(); ij++) {
    arguments.push_back((*ij)->clone());
  }
  return *this;
}


std::set<std::string> ArgumentList::getVariables() const {
  std::set<std::string> result;
  std::vector<Argument*>::const_iterator it;
  for (it = arguments.begin(); (it) != arguments.end(); it++) {
    std::set<std::string> &vars = (*it)->getVariables();
    result.insert(vars.begin(), vars.end());
  }
  return result;
}

int ArgumentList::getCount() const {
  return arguments.size();
}

void ArgumentList::addArgument(Argument *_argument) {
  arguments.push_back(_argument);
}

std::string ArgumentList::toString() const {
  std::string result;
  std::vector<Argument*>::const_iterator it;
  std::vector<Argument*>::const_iterator aux_it;
  for (it = arguments.begin(); (it) != arguments.end(); it++) {
    result += (*it)->toString();
    aux_it = it;
    aux_it++;
    if ((aux_it) != arguments.end()) {
      result += ",";
    }
  }
  return result;
}

void ArgumentList::replaceVariable(std::string old_name, int id) {
  std::vector<Argument*>::iterator it;
  for (it = arguments.begin(); (it) != arguments.end(); it++) {
    (*it)->replaceVariable(old_name, id);
  }
}

void ArgumentList::setVariableTable(VariablesTable *_vt) {
  std::vector<Argument*>::iterator it;
  for (it = arguments.begin(); (it) != arguments.end(); it++) {
    (*it)->setVariableTable(_vt);
  }
}
