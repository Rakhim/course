# pragma once
#include <vector>
#include "Predicate.h"
#include "VariablesTable.h"

class PredicateTable
{ 
  VariablesTable vt;
  std::vector<Predicate> predicates;
  
public:
  PredicateTable(void);
  ~PredicateTable(void);

  VariablesTable &variablesTable() {
    return vt;
  }
  int add(const Predicate &_predicate);
  Predicate getByID(int id) const;
  void replaceByID(const Predicate &_predicate, int ID);
  int find(const Predicate &_predicate)  const;

#ifdef DEBUG
  //debug 
  void show() {
    printf("---------------------------------\n");
    std::vector<Predicate>::const_iterator it;
    for (it = predicates.begin(); it != predicates.end(); it++) {
      printf("table[%d]: %s ", it - predicates.begin(), (*it).getName().c_str());
      printf("%s \n", (*it).getArgument().toString().c_str());
    }
  }
#endif
};
