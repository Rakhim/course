#include "Prover.h"

Prover::Prover()
{  
}

Prover::~Prover(void) {
}

bool Prover::conclusion(
  std::map<std::pair<Disjunction, Disjunction>, Disjunction> &result,
  const std::set<Disjunction> &left,
  const std::set<Disjunction> &right,
  int limit
) {
  if (limit > 0) {
    std::set<Disjunction>::const_iterator i, j;
    for (i = left.begin(); i != left.end(); i++) {
      for (j = right.begin(); j != right.end(); j++) {

        std::set<Disjunction> resolvent = i->resolvent(*j);

        if (resolvent.size()) {
          result[std::pair<Disjunction, Disjunction>(*i, *j)] = *resolvent.begin();
          if (resolvent.begin()->empty()) {
            return true;
          }
        }
      }
    }
    
    std::set<Disjunction> resolvents;
    std::map<std::pair<Disjunction, Disjunction>, Disjunction>::const_iterator it;
    for (it = result.begin(); it != result.end(); it++) {
      resolvents.insert((*it).second);
    }
      
    if (conclusion(result, left, resolvents, limit - 1) ||
        conclusion(result, right, resolvents, limit - 1)) {
      return true;
    }
  }
  return false;
} //conclusion


void Prover::addFormula(const PredicateFormula &_formula) {
  _formula.fillPredicateTable(pt);
  
  std::set<Disjunction> setOfDisjunctions = _formula
      .getSNF()
      .getSetOfDisjunctions();
  base.insert(setOfDisjunctions.begin(), setOfDisjunctions.end());
}

std::map<std::pair<Disjunction, Disjunction>, Disjunction> Prover::prove(const PredicateFormula &_formula) {
  _formula.fillPredicateTable(pt);
  std::map<std::pair<Disjunction, Disjunction>, Disjunction>
    result, result1, result2;
  SNF &snf = _formula.getSNF();
  std::set<Disjunction> negativeSentence = snf.negate().getSetOfDisjunctions();

  if (conclusion(result, base, negativeSentence, 150)) {
    return result;
  }
  // too many resolvents
  result.clear();
  return result;
}
