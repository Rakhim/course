#pragma once
#include "PredicateFormula.h"
#include "PredicateTable.h"

class Prover
{
  std::set<Disjunction> base;
  PredicateTable pt;

private:
  bool conclusion(
    std::map<std::pair<Disjunction, Disjunction>, Disjunction> &result,
    const std::set<Disjunction> &left,
    const std::set<Disjunction> &right,
    int limit
  );
public:
  Prover();
  ~Prover(void);

  void addFormula(const PredicateFormula &_formula);
  // map of resolvents for each pair of disjunctins
  std::map<std::pair<Disjunction, Disjunction>, Disjunction> prove(const PredicateFormula &_formula);

#ifdef DEBUG
  void show() const {
    printf("---------------------------------\n");
    std::set<Disjunction>::const_iterator it;
    for (it = base.begin(); it != base.end(); it++) {
      printf("base[%d]: %s\n", std::distance(base.begin(), it), (*it).toString().c_str());
    }
  }
  
  void showTable() {
    pt.show();
  }
#endif
};

