#pragma once
#include "Sentence.h"
#include "Formula.h"

class ParenthsizedFormula:
  public Formula
{
  Sentence *sentence;

public:
  ParenthsizedFormula(Sentence *_formula):
    sentence(_formula)
  {
  }
  ParenthsizedFormula(const ParenthsizedFormula &_formula) {
    sentence = _formula.sentence->clone();
  }
  ~ParenthsizedFormula(void) {
    delete sentence;
  }

  virtual CNF getCNF() const;
  virtual std::string toString() const;
  virtual std::set<std::string> getVariables() const;
  virtual ParenthsizedFormula* clone() const;
  virtual Formula* negate() const;
  virtual void fillPredicateTable(PredicateTable *_pt);
  virtual void replaceVariable(std::string old_name, int id);
  virtual void setVariableTable(VariablesTable *_vt);
};
