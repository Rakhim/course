#pragma once
#include "formula.h"

class ImplicationFormula :
  public Formula
{
  Formula *left;
  Formula *right;
  
public:
  ImplicationFormula(Formula *_left, Formula *_right);
  ImplicationFormula(const ImplicationFormula &formula);
  ~ImplicationFormula(void);
  virtual CNF getCNF() const;
  virtual std::string toString() const;
  virtual ImplicationFormula* clone() const;
  virtual std::set<std::string> getVariables() const;
  virtual Formula* negate() const;
  virtual void fillPredicateTable(PredicateTable *_pt);
  virtual void replaceVariable(std::string old_name, int id);
  virtual void setVariableTable(VariablesTable *_vt);

};
