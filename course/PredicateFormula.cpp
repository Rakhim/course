#include "PredicateFormula.h"

void PredicateFormula::constructor(std::istream &_signature, std::istream &_sentence) {
  signature.read(_signature);
  constructor(_sentence);
}

void PredicateFormula::fillPredicateTable(PredicateTable &_pt) const {
  sentence->getFormula()->fillPredicateTable(&_pt);
}

void PredicateFormula::constructor(std::istream &_sentence) {
  lexer = new Lexer();
  lexer->switch_streams(&_sentence, 0);
  parser = new Parser(lexer, sentence, &signature);

  if (parser && (1 == parser->parse())) {
    throw std::string(" syntax error ");
  }
}

PredicateFormula::PredicateFormula(const Signature &_signature, const std::string &_sentence):
  parser(0x0),
  lexer(0x0),
  sentence(0x0),
  signature(_signature)
{
  std::istringstream sentence_data(_sentence);
  try {
    constructor(sentence_data);
  } catch (std::string message) {
    delete parser;
    delete sentence;
    delete lexer;
    throw message;
  }
}

PredicateFormula::PredicateFormula(const Signature &_signature, std::istream &_sentence):
  parser(0x0),
  lexer(0x0),
  sentence(0x0),
  signature(_signature)
{
  try {
    constructor(_sentence);
  } catch (std::string message) {
    delete parser;
    delete sentence;
    delete lexer;
    throw message;
  }
}

PredicateFormula::~PredicateFormula() {
  delete lexer;
  delete parser;
  delete sentence;
}

SNF PredicateFormula::getSNF() const {
  return sentence->getPNF().getSNF();
}

std::string PredicateFormula::toString() const {
  if (sentence) {
    return sentence->toString();
  }
  return "";
}
