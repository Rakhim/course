#include "Variable.h"
#include "Constant.h"
#include "Function.h"

bool Variable::unify(const Argument * const _argument) const {
  if ((std::string(typeid(*_argument).name()) == "class Function") &&
    ((Function*)_argument)->getVariables().begin()->c_str() == identifier
  ) {
    return false;
  }
  return true;
}


Variable::Variable(const std::string &_identifier):
  identifier(_identifier),
  id(-1)
{
}

Variable::~Variable(void) {
}


int Variable::getCount() const {
  return 1;
}

std::string Variable::toString() const {
  return -1 == id ? identifier : vt->getByID(id);
}

std::set<std::string> Variable::getVariables() const {
  std::set<std::string> result;
  result.insert(identifier);
  return result;
}

void Variable::replaceVariable(std::string old_name, int _id) {
  if (identifier == old_name) {
    id = _id;
  }
}

void Variable::setVariableTable(VariablesTable *_vt) {
  vt = _vt;
}

Variable* Variable::clone() const {
  return new Variable(*this);
}