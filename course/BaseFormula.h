#pragma once
#include <string>
#include "PredicateTable.h"

class BaseFormula
{
public:
  virtual std::string toString() const = 0;
  virtual ~BaseFormula(void) {
  }
};

