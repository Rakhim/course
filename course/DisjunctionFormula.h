#pragma once
#include "formula.h"

class DisjunctionFormula :
  public Formula
{
  Formula *left;
  Formula *right;

public:
  DisjunctionFormula(Formula *_left, Formula *_right);
  DisjunctionFormula(const DisjunctionFormula &formula);
  ~DisjunctionFormula(void);
  virtual CNF getCNF() const;
  virtual std::string toString() const;
  virtual std::set<std::string> getVariables() const;
  virtual DisjunctionFormula* clone() const;
  virtual Formula* negate() const;
  virtual void fillPredicateTable(PredicateTable *_pt);
  virtual void replaceVariable(std::string old_name, int id);
  virtual void setVariableTable(VariablesTable *_vt);

};
