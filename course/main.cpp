#include <fstream>
#include "PredicateFormula.h"
#include "Prover.h"

int main(int argc, char **argv) {
  if (6 == argc)
  {
    int n = atoi(argv[3]);
    if (n < 1) {
      printf("The number of formulas must be greater than zero \n");
      return 1;
    }
    std::ifstream signature_data(argv[1]);
    std::ifstream sentences(argv[2]);
    std::ifstream hypotise(argv[4]);
    std::ofstream outfile(argv[5]);

    Signature signature;
    signature.read(signature_data);
    //------
    PredicateTable pt;
    Prover prover;
    try {
      for (int i = 0; i < n; i++) {
        PredicateFormula pf(signature, sentences);
        prover.addFormula(pf);
      }
      
      PredicateFormula pf(signature, hypotise);
      std::map<std::pair<Disjunction, Disjunction>, Disjunction> result =     
      prover.prove(pf);
      if (!result.size()) {
        outfile << "No\n";
      } else {
        outfile << "Yes\n";
      }
      
      std::map<std::pair<Disjunction, Disjunction>, Disjunction>::const_iterator it;
      for (it = result.begin(); it != result.end(); it++) {
        outfile << "1 " << (*it).first.first.toString() << ";\n";
        outfile << "2 " << (*it).first.second.toString() << ";\n";
        outfile << "|-" << (*it).second.toString() << ";\n\n";
      }
    } catch (std::string message) {
      printf("Error: %s\n", message.c_str());
    } catch (...) {}

  } else {

    int i = strlen(argv[0]) - 1;
    while((argv[0][i] != '\\') && (argv[0][i] != '/') && (i > 0)) {
      --i;
    }
    printf("\nUsage: %s %s\n", (argv[0] + i),
           "signature sentences n hypotise outfile"
    );
  }
  return 0;
}