#include "ParenthsizedFormula.h"

CNF ParenthsizedFormula::getCNF() const {
//  Formula *formula = sentence->getFormula()->clone();
//  CNF &cnf = formula->getCNF();
//  delete formula;
  return sentence->getFormula()->getCNF();
}

std::string ParenthsizedFormula::toString() const {
  return std::string("(") + sentence->toString() + std::string(")");
}

std::set<std::string> ParenthsizedFormula::getVariables() const {
  return sentence->getVariables();
}

ParenthsizedFormula* ParenthsizedFormula::clone() const {
  return new ParenthsizedFormula(*this);
}

Formula* ParenthsizedFormula::negate() const {
  return new ParenthsizedFormula(
    sentence->negate()
  );
}

void ParenthsizedFormula::fillPredicateTable(PredicateTable *_pt) {
  sentence->getFormula()->fillPredicateTable(_pt);
}

void ParenthsizedFormula::replaceVariable(std::string old_name, int id) {
  sentence->replaceVariable(old_name, id);
}

void ParenthsizedFormula::setVariableTable(VariablesTable *_vt) {
  sentence->setVariableTable(_vt);
}
