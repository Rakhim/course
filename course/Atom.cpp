#include "Atom.h"
#include "NegationFormula.h"
#include "QuantifierList.h"

Atom::Atom(const std::string &_identifier, const ArgumentList &_argument):
  identifier(_identifier),
  argument(_argument),
  id(-1)
{
}

Formula* Atom::negate() const {
  return new NegationFormula(
    new Atom(*this)
  );
}

std::string Atom::toString() const {
  return  identifier + (argument.getCount() != 0 ?
          std::string("(") + argument.toString() + std::string(")") : "");
}

int Atom::getCountOfArguments() const {
  return argument.getCount();
}
  
std::set<std::string> Atom::getVariables() const {
  return argument.getVariables();
}

Atom* Atom::clone() const {
  return new Atom(*this);
}

void Atom::fillPredicateTable(PredicateTable *_pt) {
  pt = _pt;
  argument.setVariableTable(&pt->variablesTable());
  Predicate predicate(identifier, argument);  
  id = pt->add(predicate);
}

CNF Atom::getCNF() const {
  CNF result(pt);
  if (-1 != id) {
    Disjunction disj;
    disj.insert(id, false);
    result.insert(disj);
  } else {
    printf("CNF::getCNF (Predicate is not found in the table) \n");
  }
  return result;
}

void Atom::replaceVariable(std::string old_name, int id) {
  argument.replaceVariable(old_name, id);
}

void Atom::setVariableTable(VariablesTable *_vt) {
  argument.setVariableTable(_vt);
}
