#pragma once
#include <string>

#include "BaseFormula.h"
#include "CNF.h"
#include "QuantifierList.h"

class SNF:
  public BaseFormula
{
  QuantifierList prefix;
  CNF matrix;

public:
  SNF(const QuantifierList &quantifierList, const CNF &cnf);
  ~SNF(void);

  std::set<Disjunction> getSetOfDisjunctions() const {
    return matrix.getSetOfDisjunctions();
  }

  SNF negate() const {  
    return SNF(prefix.negate(), matrix.negate());
  }
  
  virtual std::string toString() const {
    return prefix.toString() + matrix.toString();
  }

};

