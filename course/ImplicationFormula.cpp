#include "ImplicationFormula.h"
#include "NegationFormula.h"
#include "ConjunctionFormula.h"

ImplicationFormula::ImplicationFormula(Formula *_left, Formula *_right):
  left(_left),
  right(_right)
{
}

ImplicationFormula::ImplicationFormula(const ImplicationFormula &formula) {
  left = formula.left->clone();
  right = formula.right->clone();
}

ImplicationFormula::~ImplicationFormula(void) {
  delete left;
  delete right;
}
  
CNF ImplicationFormula::getCNF() const {
  return left->getCNF()
        .negate()
        .or(right->getCNF());
}

Formula* ImplicationFormula::negate() const {
  return new ConjunctionFormula(
    left,
    right->negate()
  );
}

std::string ImplicationFormula::toString() const {
  return left->toString() + std::string(" -> ") + right->toString();
}

ImplicationFormula* ImplicationFormula::clone() const {
  return new ImplicationFormula(*this);
}

std::set<std::string> ImplicationFormula::getVariables() const {
  std::set<std::string> &leftVars = left->getVariables();
  std::set<std::string> &rightVars = right->getVariables();

  std::set<std::string> result;
  result.insert(leftVars.begin(), leftVars.end());
  result.insert(rightVars.begin(), rightVars.end());
    
  return result;
}

void ImplicationFormula::fillPredicateTable(PredicateTable *_pt) {
  left->fillPredicateTable(_pt);
  right->fillPredicateTable(_pt);
}

void ImplicationFormula::replaceVariable(std::string old_name, int id) {
  left->replaceVariable(old_name, id);
  right->replaceVariable(old_name, id);
}

void ImplicationFormula::setVariableTable(VariablesTable *_vt) {
  left->setVariableTable(_vt);
  right->setVariableTable(_vt);
}

//SNF ImplicationFormula::getSNF() const {
//  return SNF();
//}