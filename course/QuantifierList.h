#pragma once
#include <vector>
#include <iterator>
#include <algorithm>
#include "Quantifier.h"

class QuantifierList
{
  std::vector<Quantifier*> quantifiers;

public:
  QuantifierList(void);
  QuantifierList(const QuantifierList &_quantifiersList);
  ~QuantifierList(void);

  QuantifierList& operator= (const QuantifierList &_ql);

  void addQuantifier(Quantifier *_quantifier);
  std::string toString() const;
  virtual std::set<std::string> getVariables() const;
  virtual QuantifierList negate() const;
  virtual void replaceVariable(std::string old_name, int id);
  virtual void setVariableTable(VariablesTable *_vt);
};
