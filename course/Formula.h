#pragma once
#include <set>

#include "Node.h"
#include "CNF.h"
#include "SNF.h"

class Formula:
  public Node
{
public:
  virtual Formula* clone() const = 0;
  virtual void fillPredicateTable(PredicateTable *_pt) = 0;
  virtual CNF getCNF() const = 0;
  virtual std::string toString() const = 0;
  virtual Formula* negate() const = 0;
  virtual ~Formula() {
  }
};