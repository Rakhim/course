#include "Constant.h"
#include "Variable.h"

bool Constant::unify(const Argument * const _argument) const {
  if (std::string(typeid(*_argument).name()) == "class Constant") {
    return ((Constant*)_argument)->identifier == identifier;
  } else if (std::string(typeid(*_argument).name()) == "class Variable") {
    return true;
  }
  return false;
}

Constant::Constant(const std::string &_identifier):
  identifier(_identifier),
  id(-1)
{
}


Constant::~Constant(void)
{
}

int Constant::getCount() const {
  return 1;
}

std::string Constant::toString() const {
  return -1 == id ? identifier : vt->getByID(id);
}

std::set<std::string> Constant::getVariables() const {
  std::set<std::string> result;
  result.insert(identifier);
  return result;
}

void Constant::replaceVariable(std::string old_name, int _id) {
  if (identifier == old_name) {
    id = _id;
  }
}

void Constant::setVariableTable(VariablesTable *_vt) {
  vt = _vt;
}

Constant* Constant::clone() const {
  return new Constant(*this);
}
