#pragma once
#include <algorithm>
#include <iterator>

#include "Sentence.h"
#include "Formula.h"
#include "QuantifierList.h"

class QuantifierSentence :
  public Sentence
{
  QuantifierList quantifier;
  Formula *formula;

private:
  int addToVariablesTableWithRename(VariablesTable &vt, const std::string &_name) const;

public:
  QuantifierSentence(const QuantifierList &_quantifier, Formula *_formula);
  QuantifierSentence(const QuantifierSentence &qs);
  ~QuantifierSentence(void);
  
  virtual QuantifierSentence* clone() const;
  virtual CNF getCNF() const;
  virtual std::string toString() const;
  virtual std::set<std::string> getVariables() const;
  virtual Sentence* negate() const;
  virtual Quantifier* getPrefix() const;
  virtual void fillPredicateTable(PredicateTable *_pt);
  virtual void replaceVariable(std::string old_name, int id);
  virtual void setVariableTable(VariablesTable *_vt);
  virtual PNF getPNF() const;
  virtual Formula* getFormula() const;
};
